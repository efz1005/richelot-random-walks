# Richelot random walks

This repo contains some of the code I've written to explore the behavior of random walks in superspecial graphs of PPAS. More specifically, it includes some functions to compute Richelot isogenies (`richelot.sage`), which are then used to generate adjacency matrices. 

## Data

Adjacency matrices of Richelot graphs are included, from p = 7 until p = 617. Each adjacency matrix is stored as a sparse matrix using numpy's npz format. Additionally, a file is saved to store the indices in the adjacency matrix that correspond to a product of elliptic curves.

## Sage Docker image

With Docker installed, run bash run.sh to build the Sage image and run it, with this folder mapped inside the container. Alternatively, the code runs on Sage 9.0 compiled with Python 3.

## References

- Toshiyuki Katsura and Katsuyuki Takashima (2020). Counting Richelot isogenies between superspecial abelian surfaces. https://www.math.auckland.ac.nz/~sgal018/ANTS/papers/Katsura-Takashima.pdf
- Craig Costello and Benjamin Smith (2020). The supersingular isogeny problem in genus 2 and beyond. https://hal.inria.fr/hal-02389073/document
- Wouter Castryck, Thomas Decru and Benjamin Smith (2019). Hash functions from superspecial genus-2 curves using Richelot isogenies. https://hal.inria.fr/hal-02067885v1/document
- E. V. Flynn, Yan Bo Ti (2019). Genus Two Isogeny Cryptography. https://eprint.iacr.org/2019/177.pdf
- Everett W. Howe, Franck Leprevost and Bjorn Poonen (2000). Large torsion subgroups of split Jacobians of curves of genus two or three. http://www-math.mit.edu/~poonen/papers/large.pdf