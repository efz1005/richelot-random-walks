from itertools import * 

def aut_ExE():
	F.<a,b,s> = FreeGroup()
	G = F / [a^2, b^2, s^2, a*b*a^(-1)*b^(-1), s*a*s*b^(-1)]
	print(G.gap().Order())
	print(G.gap().IdGroup())


def aut_E2xE2():
	F.<a,b,s> = FreeGroup()
	G = F / [a^4, b^4, s^2, a*b*a^(-1)*b^(-1), s*a*s*b^(-1)]
	print(G.gap().Order())
	print(G.gap().IdGroup())


def aut_E3xE3():
	F.<a,b,s> = FreeGroup()
	G = F / [a^6, b^6, s^2, a*b*a^(-1)*b^(-1), s*a*s*b^(-1)]
	print(G.gap().Order())
	print(G.gap().IdGroup())


def find_isomorphism_E2xE2():
	F.<a,b,s> = FreeGroup()
	G = F / [a^4, b^4, s^2, a*b*a^(-1)*b^(-1), s*a*s*b^(-1)]
	i = G(1)

	G2 = [x for x in G if x != i and x^2 == i]
	G4 = [x for x in G if x != i and x^4 == i and x^2 != i]

	print(len(G4) * len(G2) * len(G4))
	# G = < a,b,c,d | a3=b3=c4=d2=1, ab=ba, 
 	# ac=ca, ad=da, cbc-1=dbd=b-1, dcd=c-1 > 
	print("Starting randomization")
	for a,b,c in product(G4, G2, G4):
		cond = b*a*b*a == i
		cond = a*c == c*a and cond
		cond = a*c*b == b*c and cond
		if cond:
			print(a,b,c)


def find_isomorphism_E3xE3():
	F.<a,b,s> = FreeGroup()
	G = F / [a^6, b^6, s^2, a*b*a^(-1)*b^(-1), s*a*s*b^(-1)]
	i = G(1)

	G3 = [x for x in G if x != i and x^3 == i]
	G2 = [x for x in G if x != i and x^2 == i]
	G4 = [x for x in G if x != i and x^4 == i and x^2 != i]

	print(len(G3) * len(G3) * len(G2) * len(G4))
	# G = < a,b,c,d | a3=b3=c4=d2=1, ab=ba, 
 	# ac=ca, ad=da, cbc-1=dbd=b-1, dcd=c-1 > 
	print("Starting randomization")
	for a,b,c,d in product(G3, G3, G4, G2):
		cond = a*b == b*a
		cond = a*c == c*a and cond
		cond = a*d == d*a and cond
		cond = b*c*b == c and cond
		cond = d*b*d*b == i and cond
		cond = d*c*d*c == i and cond
		if cond:
			print(a,b,c,d)


"""aut_ExE()
print()

aut_E2xE2()
print()

aut_E3xE3()"""

find_isomorphism_E2xE2()