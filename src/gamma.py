import sys
import numpy as np
from math import sqrt

WALK_FOLDER = "../short_walks"

if len(sys.argv) == 1:
	exit()
else:
	p = int(sys.argv[1])

f = open(f"{WALK_FOLDER}/{p}.txt")

values = np.array(list(map(int, f.readlines())))
f.close()

mean = np.mean(values)
std = np.std(values)

gamma = 1 / mean

def q(p):
	if p in [0, 1]:
		return False
	qval = (mean - 1/p) / sqrt((1 - p) / (len(values) * p**2))
	return -1.96 < qval < 1.96


print(f"p \t\t= {p}")
print(f"p / 1.86 \t= {p * 15 / 28}")
print()
print(f"Sample size \t= {len(values)}")
print(f"Avg walk \t= {mean}")
print(f"Std walk \t= {std}")
print()
print(f"gamma \t\t= {gamma}")
print(f"gamma*p \t= {gamma*p}")

min_gamma = p*min(filter(q, np.arange(0.5/p, 5/p, 0.0001/p)))
max_gamma = p*max(filter(q, np.arange(0.5/p, 5/p, 0.0001/p)))
print(f"95% confidence interval = ({min_gamma}, {max_gamma})")
