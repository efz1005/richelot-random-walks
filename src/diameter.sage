import sys
from tqdm import tqdm
import numpy as np
from scipy.sparse import coo_matrix, save_npz, load_npz, dia_matrix, identity
from scipy.sparse.linalg import eigs
from scipy.linalg import eig as eigenvalues
from scipy.sparse.csgraph import floyd_warshall, shortest_path, dijkstra
import random
load("richelot.sage")

MATRIX_FOLDER = "../adjacency_matrices"


if len(sys.argv) > 1:
	q = int(sys.argv[1])
else:
	q = 13

if len(sys.argv) > 2:
	s = int(sys.argv[2])
else:
	s = q + 1000

for p in primes(q, s):

	try:
		A = load_npz(f"{MATRIX_FOLDER}/{p}.npz").T
		ecp = np.load(f"{MATRIX_FOLDER}/{p}_ecp.npy", allow_pickle=True)
	except:
		continue

	# Construct a matrix with the same values as A, except
	# there are zeroes at each row and column corresponding to 
	# an elliptic curve product (ecp[i] == 1)

	"""jac = np.ones(len(ecp)) - ecp
	D = dia_matrix((jac, [0]), A.shape)
	B = D * A * D
	B.eliminate_zeros()

	# Make B a left stochastic matrix
	B_col_sum = B.sum(axis=int(0))
	B_col_sum[B_col_sum == 0] = 1

	B_diag_inv = dia_matrix((np.reciprocal(B_col_sum), [0]), B.shape)

	B = B * B_diag_inv

	C = B[ecp==0,:][:,ecp==0]"""

	C = (A + identity(A.shape[0])) / 16
	
	nodes = len(ecp)
	diameter = floor(log(14 * C.shape[0] + 1) / log(15)) + 1
	print(f"p = {p}, diam >= {diameter}")

	for i in range(nodes):
		b = np.zeros(nodes)
		b[i] = 1

		fw = 0
		while np.count_nonzero(b==0) > 0:
			b = C*b
			fw += 1

		diameter = max(diameter, fw)

	
	print(f"diameter = {diameter}\n")

	#f = open(f"diameter/{p}.txt", 'w')
	#f.write(f"{diameter}")
	#f.close()
	


