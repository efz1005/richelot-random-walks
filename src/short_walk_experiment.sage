import multiprocessing as mp
import os
from tqdm import tqdm
load("richelot.sage")

WALK_FOLDER = "../short_walks"

if len(sys.argv) > 1:
	q = int(sys.argv[1])
else:
	q = 13

if len(sys.argv) > 2:
	s = int(sys.argv[2])
else:
	s = 1000000

if len(sys.argv) > 3:
	nthread = int(sys.argv[3])
else:
	nthread = 1

def pool_fn(pH):
	p, H = pH
	return walk_until_elliptic_curve_product(random_curve(p, H=H, mix=5))

primes = [
 503,
 1009,
 1511,
 2003,
 2503,
 3001,
 3511,
 4001,
 4507,
 5003,
 5501,
 6007,
 6521,
 7001,
 7507,
 8009,
 8501,
 9001,
 9511,
 10007
]

#for p in primes(q, s):
for p in primes:
	print(f"p = {p}\n")

	n = 10

	for _ in range(10):
		with mp.Pool(nthread) as pool:
			print(f"Finding starting curve...")
			H = starting_curve(p)
			print(f"Walking away from starting curve...")
			H = random_curve(p, H=H, mix=80)

			#print(f"Computing batch of {n} random curves...")
			#curves = pool.starmap(random_curve, )
			#print("Random curves done.")

			print(f"Starting batch of {n * nthread} random walks with {nthread} threads...")
			random_walks = pool.imap_unordered(
				pool_fn,
				[(p, H) for _ in range(int(n * nthread))]
			)

			progress = tqdm(random_walks, total=int(n * nthread))

			for steps in progress:
				f = open(f"{WALK_FOLDER}/{p}.txt", "a")
				f.write(f"{steps}\n")
				f.close()

			print("Random walks done.\n")
			pool.close()