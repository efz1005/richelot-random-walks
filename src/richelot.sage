from random import randint
from sage.schemes.elliptic_curves.constructor import coefficients_from_Weierstrass_polynomial
from sage.schemes.hyperelliptic_curves.jacobian_morphism import JacobianMorphism_divisor_class_field
from sage.schemes.curves.point import ProjectivePlaneCurvePoint_field
from tqdm import tqdm
from time import sleep
from itertools import permutations, islice

hash_permutations = [
	[0, 2, 1, 4, 3, 5],
	[0, 2, 1, 5, 3, 4],
	[0, 3, 1, 4, 2, 5],
	[0, 3, 1, 5, 2, 4],
	[0, 4, 1, 2, 3, 5],
	[0, 4, 1, 3, 2, 5],
	[0, 5, 1, 2, 3, 4],
	[0, 5, 1, 3, 2, 4],
]

non_hash_permutations = [
	[0, 1, 2, 4, 3, 5],
	[0, 1, 2, 5, 3, 4],
	[0, 2, 1, 3, 4, 5],
	[0, 3, 1, 2, 4, 5],
	[0, 4, 1, 5, 2, 3],
	[0, 5, 1, 4, 2, 3]
]

backtracking = [
	[0, 1, 2, 3, 4, 5]
]

all_permutations = hash_permutations + non_hash_permutations + backtracking
exploring_permutations = hash_permutations + non_hash_permutations


class EllipticCurveProduct(list):
	def __init__(self, E1, E2):
		super(EllipticCurveProduct, self).__init__([E1, E2])
		self.__invariants = None

	def absolute_igusa_invariants_kohel(self):
		if self.__invariants is None:
			j0 = self[0].j_invariant()
			j1 = self[1].j_invariant()
			self.__invariants = tuple(sorted([j0, j1]))

		return self.__invariants


class RichelotIsogeny(object):
	def __init__(self, domain, t: int):
		self.__domain = domain

		if isinstance(domain, EllipticCurveProduct):
			if t < 9:
				self.__codomain, _ = elliptic_curve_product_isogeny(domain, t)
			else:
				self.__codomain, _ = jacobian_from_elliptic_product2(domain, t - 9)
		else:
			self.__codomain, _ = richelot_isogeny(domain, t)

	def domain(self):
		return self.__domain

	def codomain(self):
		return self.__codomain

	def domain_invariants(self):
		return self.__domain.absolute_igusa_invariants_kohel()

	def codomain_invariants(self):
		return self.__codomain.absolute_igusa_invariants_kohel()

	def dual(self):
		pass


def bracket(P, Q):
	return diff(P) * Q - diff(Q) * P


def image_curve(G, points=[], h=[]):
	if len(h) > 0:
		h = []
	for i in range(3):
		h.append(bracket(G[(i+1) % 3], G[(i+2) % 3]))

	M = matrix([[0 if g.degree() == 1 else g[2], g[1], g[0]] for g in G])
	delta = det(M)

	if delta == 0:
		return image_elliptic_product(G, points)

	f = prod(h) / delta

	H = HyperellipticCurve(f)
	J = H.jacobian()

	def isogeny(D):
		if isinstance(D, ProjectivePlaneCurvePoint_field):
			print(G)
			print(h)
			u = G[0](D[0]) * h[0] + G[1](D[0]) * h[1]
			v = G[0](D[0]) * h[0] * (D[0] - h[0].variables()[0]) / (delta * D[1]) # delta...

			u /= list(u)[-1]
			v = v % u

			print((v^2 - f) % u)

			return J([u, v])
		elif isinstance(D, JacobianMorphism_divisor_class_field):
			return None # todo

	images = map(isogeny, points)

	return H, images


def jacobian_from_elliptic_product(J: EllipticCurveProduct):
	E1 = J[0]

	R.<t> = (E1.base_ring())[]

	f, _ = E1.hyperelliptic_polynomials()
	f = f(t^2)

	"""NOTICE: f(t^2) is equal to doing:
	#E2 = J[1]
	g, _ = E2.hyperelliptic_polynomials()

	f_roots = [x for x, _ in f.roots()]
	g_roots = [x for x, _ in g.roots()]

	for g_roots_perm in permutations(g_roots):
		if f_roots[0] * g_roots_perm[0] == \
			f_roots[1] * g_roots_perm[1] == \
			f_roots[2] * g_roots_perm[2]:
			g_roots = g_roots_perm
			break

	
	g = R(t^6 / list(g)[0] * g(f_roots[0] * g_roots[0] / t^2))

	print()
	print(f, HyperellipticCurve(f).absolute_igusa_invariants_kohel())
	print(g, HyperellipticCurve(g).absolute_igusa_invariants_kohel())
	print()
	"""

	return HyperellipticCurve(f)


def jacobian_from_elliptic_product2(J: EllipticCurveProduct, i=0):
	E1 = J[0]
	E2 = J[1]

	alpha = [x for x, _ in E1.division_polynomial(2).roots()]
	beta_ = [x for x, _ in E2.division_polynomial(2).roots()]

	beta = list(permutations(beta_))[i]

	delta_a = E1.discriminant()
	delta_b = E2.discriminant()

	a = [
		(alpha[2] - alpha[1])^2 / (beta[2] - beta[1]) \
		+ (alpha[1] - alpha[0])^2 / (beta[1] - beta[0]) \
		+ (alpha[0] - alpha[2])^2 / (beta[0] - beta[2]),
		alpha[0] * (beta[2] - beta[1]) \
		+ alpha[1] * (beta[0] - beta[2]) \
		+ alpha[2] * (beta[1] - beta[0])
	]

	b = [
		(beta[2] - beta[1])^2 / (alpha[2] - alpha[1]) \
		+ (beta[1] - beta[0])^2 / (alpha[1] - alpha[0]) \
		+ (beta[0] - beta[2])^2 / (alpha[0] - alpha[2]),
		beta[0] * (alpha[2] - alpha[1]) \
		+ beta[1] * (alpha[0] - alpha[2]) \
		+ beta[2] * (alpha[1] - alpha[0])
	]

	if a[1] == 0 or b[1] == 0:
		return J, []
		# TODO: computation of images of points

	A = delta_b * a[0] / a[1]
	B = delta_a * b[0] / b[1]

	R.<x> = (E1.base_ring())[]

	f = - (A * (alpha[1] - alpha[0]) * (alpha[0] - alpha[2]) * x^2 \
			+ B * (beta[1] - beta[0]) * (beta[0] - beta[2])) \
	    * (A * (alpha[2] - alpha[1]) * (alpha[1] - alpha[0]) * x^2 \
	    	+ B * (beta[2] - beta[1]) * (beta[1] - beta[0])) \
	    * (A * (alpha[0] - alpha[2]) * (alpha[2] - alpha[1]) * x^2 \
	    	+ B * (beta[0] - beta[2]) * (beta[2] - beta[1]))

	return HyperellipticCurve(f), []


def elliptic_curve_product_isogeny(J: EllipticCurveProduct, i=0):
	i1 = i % 3
	i2 = i // 3

	E1 = J[0]
	E2 = J[1]

	P1_list = E1(0).division_points(2)
	P1_list.remove(E1(0))

	P2_list = E2(0).division_points(2)
	P2_list.remove(E2(0))

	P1 = P1_list[i1]
	P2 = P2_list[i2]

	return EllipticCurveProduct(E1.isogeny(P1).codomain(), E2.isogeny(P2).codomain()), []


def image_elliptic_product(G, points=[]):
	F = G[0].base_ring()

	R.<x> = PolynomialRing(F)

	# ensure G[0] is of degree 2
	if G[0].degree() == 1 :
		G[0], G[1] = G[1], G[0]
	
	#l, m = pass #put G[2] as sum of G[0] and G[1]

	# M = matrix([[0 if g.degree() == 1 else g[2], g[1], g[0]] for g in G])
	M = matrix([
		[G[0][0], G[0][1], 0 if G[0].degree() == 1 else G[0][2]],
		[G[1][0], G[1][1], 0 if G[1].degree() == 1 else G[1][2]],
	]).T

	b = vector([G[2][0], G[2][1], F(0) if G[2].degree() == 1 else G[2][2]])
	try:
		sol = M.solve_right(b)
		l, m = sol
	except:
		print(M)
		print(b)

	alpha_disc = (G[0][1] + x * G[1][1]) ^ 2 - 4 * (G[0][2] + x * G[1][2]) * (G[0][0] + x * G[1][0])
	(a1, _), (a2, _) = alpha_disc.roots()

	xx1 = (G[0] + a1 * G[1])
	xx2 = (G[0] + a2 * G[1])
	
	n1 = xx1[2]
	n2 = xx2[2]

	linear_combination = matrix(
		[[1, a1], 
		 [1, a2]]
		#[[1 / n1, a1 / n1], 
		# [1 / n2, a2 / n2]]
	).inverse()

	A = matrix([[1, 0], [0, 1], [l, m]]) * linear_combination

	leading1 = A[0][0] * A[1][0] * A[2][0]
	leading2 = A[0][1] * A[1][1] * A[2][1]

	R.<u, v> = F[]

	p1 = (u + A[0][1] * A[1][0] * A[2][0]) \
		* (u + A[1][1] * A[0][0] * A[2][0]) \
		* (u + A[2][1] * A[0][0] * A[1][0])

	E1 = EllipticCurve((v*v - p1))
	
	p2 = (A[0][0] * A[1][1] * A[2][1] + u) \
		* (A[1][0] * A[0][1] * A[2][1] + u) \
		* (A[2][0] * A[0][1] * A[1][1] + u)

	E2 = EllipticCurve((v*v - p2))

	def isogeny(P):
		(s1, _), = xx1.roots()
		(s2, _), = xx2.roots()
		
		P1 = (
			(P[0] - s1 * P[2])^2 * (P[0] - s2 * P[2]),
			P[1],
			(P[0] - s2 * P[2])^3 / leading1
		)

		P2 = (
			(P[0] - s2 * P[2])^2 * (P[0] - s1 * P[2]),
			P[1],
			(P[0] - s1 * P[2])^3 / leading2
		)

		return (E1(P1), E2(P2))

	images = map(isogeny, points)

	return EllipticCurveProduct(E1, E2), images


def richelot_isogeny(H, i=0, points=[], factors=[], h=[]):
	f, _ = H.hyperelliptic_polynomials()

	if len(factors) not in [5, 6]:
		factors = [g for g, _ in factor(f)]
	
	if len(factors) == 5:
		factors.append(1)

	perm = all_permutations[i]

	G = [
		list(f)[-1] * factors[perm[0]] * factors[perm[1]],
		factors[perm[2]] * factors[perm[3]],
		factors[perm[4]] * factors[perm[5]]
	]

	return image_curve(G, points=points, h=h)

def explore_superspecial_graph(p):
	# estimate = int(p^3  / 2880 + p^2 / 120 + p^2 / 288)

	# print(f"p = {p}")
	# print(f"p^3  / 2880 + p^2 / 120 + p^2 / 288 = {estimate}")

	F.<a> = GF(p ** 2)
	R.<x> = PolynomialRing(F)
	H = starting_curve(p)

	visited = set()
	queue = [H]

	#progress = tqdm()
	while len(queue) > 0:
		H = queue.pop(0)
		
		if H.absolute_igusa_invariants_kohel() not in visited:
			#progress.update()
			visited.add(H.absolute_igusa_invariants_kohel())

			for i in range(14):
				J, _ = richelot_isogeny(H, i)
				if isinstance(J, EllipticCurveProduct):
					if (J[0].j_invariant(), J[1].j_invariant()) not in visited \
						and (J[1].j_invariant(), J[0].j_invariant()) not in visited:
						visited.add((J[0].j_invariant(), J[1].j_invariant()))
				else:
					if J.absolute_igusa_invariants_kohel() not in visited:
						queue.append(J)

	#progress.close()

	return len(visited)


def starting_curve(p, randomize=False):
	F = GF(p ** 2)
	R.<x> = PolynomialRing(F)

	if not randomize:
		if p % 8 in [5, 7]:
			f = x ** 5 - x
			return HyperellipticCurve(f)
		elif p % 3 == 2:
			f = x ** 6 + 1
			return HyperellipticCurve(f)
		elif kronecker(-3, p) == -1:
			f = x ** 6 - 1
			return HyperellipticCurve(f)
		elif kronecker(-2, p) == -1:
			f = x * (x ** 4 - 1)
			return HyperellipticCurve(f)
		elif p % 5 == 4:
			f = x ** 5 - 1
			return HyperellipticCurve(f)
		elif p % 4 == 3:
			E = EllipticCurve(j=F(1728))
			return EllipticCurveProduct(E, E)
	
	found_supersingular_curve = False
	while not found_supersingular_curve:
		E = EllipticCurve(j=F.random_element())
		found_supersingular_curve = E.is_supersingular()

	return EllipticCurveProduct(E, E)


def starting_curve_available(p):
	return True
	# return (p % 8 in [5, 7]) or (p % 3 == 2) or (p % 4 == 3)


def random_variety(p, mix=40):
	H = starting_curve(p)

	for _ in range(int(mix * log(p))):
		i = randint(0, 14)
		H = RichelotIsogeny(H, i).codomain()

	return H

def random_curve(p, H=None, mix=40):
	if H is None:
		H = starting_curve(p)

	for _ in range(int(mix * log(p))):
		elliptic_curve_product = True

		while elliptic_curve_product:
			i = randint(0, 14)
			J = RichelotIsogeny(H, i).codomain()

			elliptic_curve_product = isinstance(J, EllipticCurveProduct)

		H = J

	return H


def walk_until_elliptic_curve_product(H):
	walk_length = 0

	elliptic_curve_product = isinstance(H, EllipticCurveProduct)

	while not elliptic_curve_product:
		i = randint(0, 14)
		H = RichelotIsogeny(H, i).codomain()
		elliptic_curve_product = isinstance(H, EllipticCurveProduct)
		walk_length += 1

	return walk_length


def walk_until_loop(H):
	walk_length = 0

	I = H
	J = None
	while J != H:
		elliptic_curve_product = True

		while elliptic_curve_product:
			try:
				i = randint(0, 14)
				J, _ = richelot_isogeny(I, i)

				elliptic_curve_product = isinstance(J, EllipticCurveProduct)
			except:
				continue
		I = J
		walk_length += 1
		print(walk_length)

	return walk_length


def jacobian_count(p):
	return (p**3 + 24*p**2 + 141*p - 346) // 2880 + 1

def ecp_count(p):
	eps = 0
	if p % 3 == 2:
		eps += 1
	if p % 4 == 3:
		eps += 1

	return int(0.5 * ((p - 1)//12 + eps) * ((p - 1)//12 + 1 + eps))

def tag(n, ecp):
	if ecp == 0 and n >= 4:
		return "0"
	elif ecp == 0 and n <= 3:
		return "Z5"
	elif ecp == 1:
		return "Z2"
	elif ecp == 3:
		return "S3"
	elif ecp == 2:
		return "Z2 x Z2"
	elif ecp == 4:
		return "D12"
	else: # ecp == 6
		return "S4"

def curve_tag(H):
	invariants = set()
	ecp = 0
	for t in range(15):
		phi = RichelotIsogeny(H, t)
		invariants.add(tuple(sorted(phi.codomain_invariants())))
		ecp += len(RichelotIsogeny(H, t).codomain_invariants()) == 2

	return tag(len(invariants), ecp)


def neighborhood_tag(H):
	current_tag = curve_tag(H)

	tags = []

	for i in range(15):
		phi = RichelotIsogeny(H, i)
		if len(phi.codomain_invariants()) == 2:
			continue
		
		I = phi.codomain()
		tags.append(curve_tag(I))

	return ": ".join([current_tag, ", ".join(sorted(tags))])


def has_extra_involutions(H):
	A, B, C, D = H.clebsch_invariants()
	A11 = 2 * C + A * B / 3
	A22 = D
	A33 = B * D / 2 + 2 * C * (B^2 + A * C) / 9
	A23 = B * (B^2 + A * C) / 3 + C * (2 * C + A * B / 3) / 3
	A31 = D
	A12 = 2 * (B^2 + A * C) / 3

	twoRsq = det(matrix([
		[A11, A12, A31],
		[A12, A22, A23],
		[A31, A23, A33]
	]))

	return twoRsq == 0 and A11*A22 != A12

