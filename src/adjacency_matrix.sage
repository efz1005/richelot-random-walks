import sys
from tqdm import tqdm
import numpy as np
from scipy.sparse import coo_matrix, save_npz, load_npz
from scipy.sparse.linalg import eigs
from collections import deque
import multiprocessing as mp
from itertools import cycle, product
load("richelot.sage")

MATRIX_FOLDER = "../adjacency_matrices"


def mapped_richelot_isogeny(Ht):
	phi = RichelotIsogeny(*Ht)
	return phi.domain(), \
			phi.domain_invariants(), \
			phi.codomain(), \
			phi.codomain_invariants()


if len(sys.argv) > 1:
	q = int(sys.argv[1])
else:
	q = 13

if len(sys.argv) > 2:
	s = int(sys.argv[2])
else:
	s = q + 1000

evs = []

for p in primes(q, s):
	print(f"p = {p}")

	jacobians = jacobian_count(p)
	elliptic_products = ecp_count(p)
	node_estimate = jacobians + elliptic_products

	print(f"node count estimate = {node_estimate}\n")

	try:
		ecp = np.load(f"{MATRIX_FOLDER}/{p}_ecp.npy")
		A = load_npz(f"{MATRIX_FOLDER}/{p}.npz")
	except:

		if not starting_curve_available(p):
			print("No starting curve is available (yet!)")
			exit()

		H = starting_curve(p)

		initial_invariant = H.absolute_igusa_invariants_kohel()

		nodes = [H] #deque([H])

		i = 0

		rows = []
		cols = []

		progress = tqdm(total=int(15*node_estimate))

		node_invariants_dict = {}
		node_invariants_dict[initial_invariant] = 0

		pool = mp.Pool(2)

		while len(nodes) > 0:#i < len(node_invariants_dict):
			phi_iterator = pool.imap_unordered(
				mapped_richelot_isogeny, 
				product(nodes, range(15))
			)

			new_nodes = []
			for H, H_invariants, I, I_invariants in phi_iterator:
				if I_invariants in node_invariants_dict:
					j = node_invariants_dict[I_invariants]
				else:
					j = len(node_invariants_dict)
					new_nodes.append(I)
					node_invariants_dict[I_invariants] = j

				i = node_invariants_dict[H_invariants]

				rows.append(i)
				cols.append(j)
				progress.update()
			nodes = new_nodes
		progress.close()
		pool.close()

		data = np.ones(len(rows))
		A = coo_matrix((data, (rows, cols))).asfptype().tocsr()

		ecp = np.zeros(len(node_invariants_dict))
		for inv, index in node_invariants_dict.items():
			ecp[index] = int(len(inv) == 2)

		save_npz(f"{MATRIX_FOLDER}/{p}.npz", A, compressed=True)
		np.save(f"{MATRIX_FOLDER}/{p}_ecp", ecp)

	print(f"actual node count = {A.shape[0]}")
	print(f"elliptic products = {sum(ecp)}")

	a, _ = eigs(A, k=2, which="LR", return_eigenvectors=False)
	(b,) = eigs(A, k=1, which="SR", return_eigenvectors=False)
	print(abs(a), abs(b))

	print('')

