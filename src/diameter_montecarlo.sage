import sys
from tqdm import tqdm
import numpy as np
from scipy.sparse import coo_matrix, save_npz, load_npz
from scipy.sparse.linalg import eigs
from collections import deque
import multiprocessing as mp
from itertools import cycle, product
import random
load("richelot.sage")

MATRIX_FOLDER = "../adjacency_matrices"

if len(sys.argv) > 1:
	q = int(sys.argv[1])
else:
	q = 13

if len(sys.argv) > 2:
	s = int(sys.argv[2])
else:
	s = q + 1000

def bfs_length(source, dest):
	queue = [(source, 0)]
	enqueued = set([source.absolute_igusa_invariants_kohel()])
	
	#first_row = A.getrow(source).nonzero()[1].tolist()
	#queue = [(node, 1) for node in first_row]
	#enqueued = set(first_row)

	curr = source
	depth = 0
	while len(queue) > 0:
		curr, depth = queue.pop(0)
		#print(len(queue))
		for t in range(15):
			phi = RichelotIsogeny(curr, t)
			inv = phi.codomain_invariants()
			if inv == dest.absolute_igusa_invariants_kohel():
				return depth + 1
			if inv not in enqueued:
				queue.append((phi.codomain(), depth + 1))
				enqueued.add(inv)
	return depth


def pair(p):
	source = random_variety(p, mix=5)
	dest = random_variety(p, mix=5)
	while dest.absolute_igusa_invariants_kohel() == source.absolute_igusa_invariants_kohel():
		dest = random_variety(p, mix=10)

	return bfs_length(source, dest)


for p in primes(q, s):
	#try:
	#	ecp = np.load(f"{MATRIX_FOLDER}/{p}_ecp.npy")
	#	A = load_npz(f"{MATRIX_FOLDER}/{p}.npz")
	#except:
	#	continue

	

	nodes = jacobian_count(p) + ecp_count(p) # len(ecp)
	diameter = floor(log(14 * nodes + 1) / log(15)) + 1
	print(f"p = {p}, diam >= {diameter}")

	print(diameter)

	with mp.Pool() as pool:
		pairs = pool.imap_unordered(
			pair, [p for _ in range(p)]
		)

		progress = tqdm(pairs, total=int(p))
		for length in progress:
			if length > diameter:
				progress.write(str(length))
				diameter = length

		print(f"diam >= {diameter}")
		print()
