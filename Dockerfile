FROM sagemath/sagemath:9.0-py3

RUN sage --python -m pip install tqdm

WORKDIR /home/sage/richelot
ENTRYPOINT bash
